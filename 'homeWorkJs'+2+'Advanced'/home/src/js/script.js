const swiper = new Swiper(".slider-container");
const menu = document.querySelector('.header__menu');
const iconMenu = document.querySelector('.header__block');
const transformBurgerBlock = document.querySelector('.header__block-burger')
const transformBurger = document.querySelector('.header__button-burger')


iconMenu.addEventListener('click', hidenMenu);



function hidenMenu() {
    if (menu.classList.contains('header__menu-active')) {
        menu.classList.remove('header__menu-active');
        transformBurger.classList.remove('header__button-burger-active');
        transformBurgerBlock.classList.remove('header__block-burger-active');
    }else{
        menu.classList.add('header__menu-active')
        transformBurgerBlock.classList.add('header__block-burger-active');
        transformBurger.classList.add('header__button-burger-active');
    }
    
}