const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

function distJs() {
    return gulp.src('src/js/*.js')
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
}

exports.readyJs = distJs;