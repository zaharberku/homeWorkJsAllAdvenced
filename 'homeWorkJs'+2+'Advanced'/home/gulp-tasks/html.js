const gulp = require('gulp');
const fileInclud = require('gulp-file-include');
const htmlmin = require('gulp-htmlmin');


function htmlFile() {
    return gulp.src('src/html/index.html')
    .pipe(fileInclud())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'))
}


exports.html = htmlFile;